import PokeShape from '../components/PokeShape';

describe('PokeShape', () => {
  it('PokeShape constructor', () => {
    const pokeShape = new PokeShape(100, 100, 'http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/128/Accept-icon.png');
    const x = pokeShape.x;
    expect(x).toBe(100);
  });

  it('PokeShape resize check', () => {
    const pokeShape = new PokeShape(100, 100, 'http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/128/Accept-icon.png');
    const x = pokeShape.checkIfResizing(100, 100);
    expect(x).toBe(0);
  });
});
