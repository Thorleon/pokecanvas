const constants = {
  M_PI: Math.PI * 2,
  RESIZERRADIUS: 8,
  FONTBASE: 1000,
  FONTSIZE: 70,
  TEXTBORDERRADIUS: 10
};
constants.ANCHORSURFACE = constants.RESIZERRADIUS * constants.RESIZERRADIUS;
constants.FONTRATIO = constants.FONTSIZE / constants.FONTBASE;
Object.freeze(constants);
export default constants;