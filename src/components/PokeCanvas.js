import PokeShape  from './PokeShape';
import PokeName from './PokeName';

import constants from '../constants';

class PokeCanvas {

  constructor() {
    this.img = null;
    this.canvas = document.getElementById("canvas");
    this.ctx = this.canvas.getContext("2d");
    this.BB = this.canvas.getBoundingClientRect();
    this.offsetX = this.BB.left;
    this.offsetY = this.BB.top;
    this.WIDTH = this.BB.width;
    this.HEIGHT = this.BB.height;

    this.dragok = false;
    this.resizeOk = false;
    this.hideAnchor = false;

    this.startX = 0;
    this.startY = 0;

    this.rects = [];
    this.rects.push(new PokeShape(150, 180, '/static/pokeball.png'));
    this.rects.push(new PokeShape(350, 190, '/static/camera-backpack-icons.png'));
    this.rects.push(new PokeShape(350, 20, '/static/run-icon.png'));
    this.rects.push(new PokeShape(20, 20, '/static/av-icon.png'));
    this.rects.push(new PokeName(150, 60, this.ctx));


    const imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', (e) => this.handleImage(e), false);

    const nameInput = document.getElementById('nameInput');
    nameInput.addEventListener('input', (e) => this.typeHandler(e));

    const saveButton = document.getElementById('saveCanvas');
    saveButton.addEventListener('click', (e) => this.save(e, saveButton));

    document.addEventListener('imageLoadedEvent', (e) => this.draw(e));

    this.draw();

    this.canvas.onmousedown = (e) => this.myDown(e);
    this.canvas.onmouseup = (e) => this.myUp(e);
    this.canvas.onmousemove = (e) => this.myMove(e);
  }

  handleImage(e) {
    const reader = new FileReader();
    reader.onload = (event) => {
      this.img = new Image();
      this.img.onload = () => {
        this.canvas.width = this.img.width;
        this.canvas.height = this.img.height;
        this.WIDTH = this.canvas.width;
        this.HEIGHT = this.canvas.height;
        this.draw();
      };
      this.img.src = event.target.result;
    };
    reader.readAsDataURL(e.target.files[0]);
  }

  roundRect(r, radius = constants.TEXTBORDERRADIUS) {
    this.ctx.beginPath();
    this.ctx.moveTo(r.x + radius, r.y);
    this.ctx.lineTo(r.x + r.width - radius, r.y);
    this.ctx.quadraticCurveTo(r.x + r.width, r.y, r.x + r.width, r.y + radius);
    this.ctx.lineTo(r.x + r.width, r.y + r.height - radius);
    this.ctx.quadraticCurveTo(r.x + r.width, r.y + r.height, r.x + r.width - radius, r.y + r.height);
    this.ctx.lineTo(r.x + radius, r.y + r.height);
    this.ctx.quadraticCurveTo(r.x, r.y + r.height, r.x, r.y + r.height - radius);
    this.ctx.lineTo(r.x, r.y + radius);
    this.ctx.quadraticCurveTo(r.x, r.y, r.x + radius, r.y);
    this.ctx.closePath();
    this.ctx.fillStyle = r.fill;
    this.ctx.fill();
  }

  clear() {
    this.ctx.clearRect(0, 0, this.WIDTH, this.HEIGHT);
  }

  draw() {
    this.clear();
    if (this.img !== null) {
      this.ctx.drawImage(this.img, 0, 0);
    }
    for (let i = 0; i < this.rects.length; i++) {
      let r = this.rects[i];
      if (r instanceof PokeShape) {
        if (r.img !== undefined) {
          this.ctx.drawImage(r.img, r.x, r.y, r.img.width, r.img.height);
        }
      }
      else {
        this.roundRect(r);
        this.ctx.font = `${r.fontSize}px Arial`;
        this.ctx.fillStyle = '#FFF';
        this.ctx.fillText(r.text, r.x + r.width * 0.5 - this.ctx.measureText(r.text).width * 0.5, r.y + r.height * 0.5 + r.fontSize * 0.5);
      }
      this.drawDragAnchor(r.x, r.y);
      this.drawDragAnchor(r.x + r.width, r.y);
      this.drawDragAnchor(r.x, r.y + r.height);
      this.drawDragAnchor(r.x + r.width, r.y + r.height);
    }
  }

  drawDragAnchor(x, y) {
    if (!this.hideAnchor) {
      this.ctx.fillStyle = '#000';
      this.ctx.beginPath();
      this.ctx.arc(x, y, constants.RESIZERRADIUS, 0, constants.M_PI, false);
      this.ctx.closePath();
      this.ctx.fill();
    }
  }

  myDown(e) {

    e.preventDefault();
    e.stopPropagation();

    const mx = parseInt(e.clientX - this.offsetX);
    const my = parseInt(e.clientY - this.offsetY);

    for (let i = 0; i < this.rects.length; i++) {
      const r = this.rects[i];
      if (r.checkIfResizing(mx, my) > -1) {
        this.resizeOk = true;
      }
      else if (mx > r.x && mx < r.x + r.width && my > r.y && my < r.y + r.height) {
        this.dragok = true;
        r.isDragging = true;
      }
    }

    this.startX = mx;
    this.startY = my;
  }

  myUp(e) {
    e.preventDefault();
    e.stopPropagation();

    this.dragok = false;
    this.resizeOk = false;
    for (let i = 0; i < this.rects.length; i++) {
      this.rects[i].isDragging = false;
      this.rects[i].isResizing = -1;
    }
  }

  myMove(e) {

    if (this.resizeOk) {

      e.preventDefault();
      e.stopPropagation();

      const mx = parseInt(e.clientX - this.offsetX);
      const my = parseInt(e.clientY - this.offsetY);

      for (let i = 0; i < this.rects.length; i++) {
        let r = this.rects[i];
        if (r.isResizing > -1) {
          r.resizeShape(mx, my);
        }
      }

      this.draw();

    } else if (this.dragok) {

      e.preventDefault();
      e.stopPropagation();

      const mx = parseInt(e.clientX - this.offsetX);
      const my = parseInt(e.clientY - this.offsetY);

      const dx = mx - this.startX;
      const dy = my - this.startY;
      for (let i = 0; i < this.rects.length; i++) {
        let r = this.rects[i];
        if (r.isDragging) {
          r.x += dx;
          r.y += dy;
          r.right += dx;
          r.bottom += dy;
        }
      }
      this.startX = mx;
      this.startY = my;

      this.draw();

    }
  }

  typeHandler(e) {
    this.rects[this.rects.length - 1].updateText(e.target.value);
    this.draw();
  }

  save(e, saveButton) {
    this.hideAnchor = true;
    this.draw();
    const saveImage = this.canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    saveButton.href = saveImage;
    this.hideAnchor = false;
    this.draw();
  }
}

export default PokeCanvas;
