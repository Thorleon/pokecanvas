import constants from '../constants';

class PokeName {

  constructor(x, y, ctx) {
    this.x = x;
    this.y = y;
    this.fill = "rgba(32, 45, 21, 0.3)";
    this.text = "Type your text".toUpperCase();
    this.textLenght = this.text.length;
    this.width = this.textLenght * 10;
    this.height = 30;
    this.right = x + this.width;
    this.bottom = y + this.height;
    this.isDragging = false;
    this.isResizing = -1;
    this.fontVar = 0;
    this.fontSize = constants.FONTSIZE + this.fontVar;
    this.ctx = ctx;
    //this.width = this.getTextWidth() + 2 * this.fontSize;
  }

  getTextWidth() {
    return this.ctx.measureText(this.text).width;
  }

  updateText(newText) {
    this.text = newText.toUpperCase();
    this.width = this.getTextWidth() + 2 * this.fontSize;
    this.right = this.x + this.width;
  }

  updateFontSize(fontSize) {
    const fontSizeInputValue = Number.parseInt(fontSize);
    if (!Number.isNaN(fontSizeInputValue)) {
      console.log(this.fontSize, this.fontVar, fontSizeInputValue);
      if (fontSizeInputValue >= this.fontVar) {
        this.fontSize += fontSizeInputValue - this.fontVar;
        this.height += fontSizeInputValue - this.fontVar;
      }
      else {
        this.fontSize = this.fontSize - (this.fontVar - fontSizeInputValue);
        this.height -= this.fontVar - fontSizeInputValue;
      }
      this.fontVar = fontSizeInputValue;
      console.log(this.width, this.fontSize);
      this.width = this.getTextWidth() + 2 * this.fontSize;
      console.log(this.width, this.fontSize);
      this.right = this.x + this.width;
      this.bottom = this.y + this.height;
    }
  }

  checkIfResizing(mx, my) {
    let dx, dy;
    dx = mx - this.x;
    dy = my - this.y;
    if (dx * dx + dy * dy <= constants.ANCHORSURFACE) {
      this.isResizing = 0;
      return 0;
    }
    // top-right
    dx = mx - this.right;
    dy = my - this.y;
    if (dx * dx + dy * dy <= constants.ANCHORSURFACE) {
      this.isResizing = 1;
      return 1;
    }
    // bottom-right
    dx = mx - this.right;
    dy = my - this.bottom;
    if (dx * dx + dy * dy <= constants.ANCHORSURFACE) {
      this.isResizing = 2;
      return 2;
    }
    // bottom-left
    dx = mx - this.x;
    dy = my - this.bottom;
    if (dx * dx + dy * dy <= constants.ANCHORSURFACE) {
      this.isResizing = 3;
      return 3;
    }

    return -1;
  }

  resizeShape(mx, my) {
    switch (this.isResizing) {
      case 0:
        this.x = mx;
        this.y = my;
        this.width = this.right - mx;
        this.height = this.bottom - my;
        break;
      case 1:
        this.y = my;
        this.width = mx - this.x;
        this.height = this.bottom - my;
        break;
      case 2:
        this.width = mx - this.x;
        this.height = my - this.y;
        break;
      case 3:
        this.x = mx;
        this.width = this.right - mx;
        this.height = my - this.y;
        break;
    }

    this.right = this.x + this.width;
    this.bottom = this.y + this.height;
    this.fontSize = constants.FONTRATIO * this.width + this.fontVar;
  }
}

export default PokeName;