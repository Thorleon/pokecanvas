import constants from '../constants';

class PokeName {

  constructor(x, y, ctx) {
    this.x = x;
    this.y = y;
    this.ctx = ctx;
    this.fill = "rgba(0, 0, 0, 0.7)";
    this.text = "Type your text".toUpperCase();
    this.textLenght = this.text.length;
    this.width = this.getTextWidth(this.text) + 10;
    this.height = 30;
    this.right = x + this.width;
    this.bottom = y + this.height;
    this.isDragging = false;
    this.isResizing = -1;
    this.fontSize = constants.FONTRATIO * this.width;
  }

  getTextWidth() {
    return this.ctx.measureText(this.text).width;
  }

  updateText(newText) {
    this.text = newText.toUpperCase();
    this.width = this.getTextWidth() + 2 * this.fontSize;
    this.right = this.x + this.width;
  }

  checkIfResizing(mx, my) {
    let dx, dy;
    dx = mx - this.x;
    dy = my - this.y;
    if (dx * dx + dy * dy <= constants.ANCHORSURFACE) {
      this.isResizing = 0;
      return 0;
    }
    // top-right
    dx = mx - this.right;
    dy = my - this.y;
    if (dx * dx + dy * dy <= constants.ANCHORSURFACE) {
      this.isResizing = 1;
      return 1;
    }
    // bottom-right
    dx = mx - this.right;
    dy = my - this.bottom;
    if (dx * dx + dy * dy <= constants.ANCHORSURFACE) {
      this.isResizing = 2;
      return 2;
    }
    // bottom-left
    dx = mx - this.x;
    dy = my - this.bottom;
    if (dx * dx + dy * dy <= constants.ANCHORSURFACE) {
      this.isResizing = 3;
      return 3;
    }

    return -1;
  }

  resizeShape(mx, my) {
    switch (this.isResizing) {
      case 0:
        this.x = mx;
        this.y = my;
        this.width = this.right - mx;
        this.height = this.bottom - my;
        break;
      case 1:
        this.y = my;
        this.width = mx - this.x;
        this.height = this.bottom - my;
        break;
      case 2:
        this.width = mx - this.x;
        this.height = my - this.y;
        break;
      case 3:
        this.x = mx;
        this.width = this.right - mx;
        this.height = my - this.y;
        break;
    }

    this.right = this.x + this.width;
    this.bottom = this.y + this.height;
    this.fontSize = constants.FONTRATIO * this.width;
  }
}

export default PokeName;