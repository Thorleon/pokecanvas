# POKECANVAS #

The goal for this app is to simple add elements from Pokemon Go panel on chosen image.

User can drag elements, resize them and even type his own text.

Example:

![picture](examples/poke-canvas.png)

Built by using ES6 and Canvas.

## Run guide: ##
```
#!bash

npm install
npm build:dev
node server.js
```

Tested on Chrome and Firefox. Few simple automatic tests powered by Jasmine. 

WARNING
Image dimensions shouldn't be too wide. Best works with 960x540px.